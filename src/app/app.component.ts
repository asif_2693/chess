import { Component , ViewChild, AfterViewInit } from '@angular/core';

import { ChessBoard, PieceColor, PlayerType } from '../ngx-chess/src/ngx-chess';
import { CHESSJS_CHESS_GAME_PROVIDERS } from '../ngx-chess/plugins/chessjs/src/chessjs';
@Component({
  selector: 'app-root',
  providers: [ ...CHESSJS_CHESS_GAME_PROVIDERS ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'chessProj';
  @ViewChild('board') private board: ChessBoard; 
  ngAfterViewInit() {
    this.board.ctrl
      .init()
      .then( () => {
        this.board.ctrl
          .setPlayer(PieceColor.BLACK, PlayerType.HUMAN)
          .setPlayer(PieceColor.WHITE, PlayerType.HUMAN)
          .newGame();
      });
  }
}
