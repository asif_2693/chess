import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  RouterModule,
  PreloadAllModules
} from '@angular/router';
import { MaterialModule } from './material';
import { AppComponent } from './app.component';
import {DomSvgBoardModule} from '../ngx-chess/plugins/dom-svg-board/src/dom-svg-board';
import { GameComponent } from './game/game.component';
import { ROUTES } from './app.route';
@NgModule({
  declarations: [
    AppComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    DomSvgBoardModule,
    RouterModule.forRoot(ROUTES),
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
